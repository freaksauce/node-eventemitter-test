'user strict';

var request = require('superagent');
var EventEmitter = require('events').EventEmitter;

module.exports = new EventEmitter();

request.get('http://www.telize.com/geoip?callback=')
		.end(function(res) {
			module.exports.emit('ready', res);
		});

